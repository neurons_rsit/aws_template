export const config : conf = {
  comp_name: 'RSIT Skynet',
  src: 'https://spinnerei-tour-2021.s3.eu-central-1.amazonaws.com/index.html',
  adresse: {
    name: 'RSIT Skynet',
    street: 'Pforzheimerstrasse',
    code: '76275',
    city: 'Ettlinen',
    googlestring: 'Pforzheimerstrasse+128a,+76275+Ettlingen'
  },
  hours: {
    monday: '07:00 - 18:00',
    tuesday: '07:00 - 18:00',
    wednesday: '07:00 - 18:00',
    thursday: '07:00 - 18:00',
    friday: '07:00 - 18:00',
    saturday: 'geschlossen',
    sunday: 'geschlossen',
  },
  register: 'HRB 731529',
  court: 'Amtsgericht Mannheim',
  company_owner: 'Rico Sonntag',
  phonenb: '+49 (0)152 33957356',
  email: 'rs@ricosnntagit.com'
}

export type conf = {
  comp_name: string
  src: string,
  adresse: adresse
  hours: hours,
  register: string,
  court: string,
  company_owner: string,
  phonenb: string,
  email: string
}

export type adresse = {
  name: string,
  street: string,
  code: string
  city: string,
  googlestring: string
}
export type hours = {
  monday: string,
  tuesday: string,
  wednesday: string,
  thursday: string,
  friday: string,
  saturday: string,
  sunday: string,
}
