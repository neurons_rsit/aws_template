import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { adresse, config, hours } from 'src/config';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  name = config.comp_name;
  adresse: adresse = config.adresse;
  apiLoaded: boolean = false;
  hours: hours = config.hours;
  phone = config.phonenb;
  email = config.email;

  constructor(private sanitizer: DomSanitizer) {

  }
  safeUrl = (): SafeHtml => {
    return this.sanitizer.bypassSecurityTrustResourceUrl(config.src);
  }

  safeGoogleUrl = (): SafeHtml => {
    return this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.google.com/maps/embed/v1/place?key=AIzaSyCxq_h3YetfRthHlBL22BU_V-Sxj9QXq6o&q=${config.adresse.googlestring}`);
  }



}
