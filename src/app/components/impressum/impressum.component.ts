import { Component, OnInit } from '@angular/core';
import { adresse, config } from 'src/config';

@Component({
  selector: 'app-impressum',
  templateUrl: './impressum.component.html',
  styleUrls: ['./impressum.component.scss']
})
export class ImpressumComponent implements OnInit {
  name = config.comp_name;
  adresse: adresse = config.adresse;
  register = config.register !== '' ? config.register : 'kein Eintrag';
  court = config.court !== '' ? config.court : 'kein';
  owner = config.company_owner;
  phonenb = config.phonenb;
  email = config.email;


  constructor() { }

  ngOnInit(): void {
  }

}
